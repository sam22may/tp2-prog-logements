<?php

require 'class/ventes.php';
require 'class/locations.php';

// aller chercher tout les logements selon le type dans la BD et appliquer la classe
function getLogements($bdd, $type){
    try{
        $requete = $bdd->query('SELECT * FROM ' . $type);
        $logements = $requete->fetchAll(PDO::FETCH_CLASS, $type);
    
        return $logements;
    } catch (PDOException $e){
        echo $e->getMessage();
    }
}

// affichage de tout les logements
function renderLogements($bdd){
    $msg_sup = "";
    //va chercher les logements par type
    $logements_ventes = getLogements($bdd, "ventes");
    $logements_locations = getLogements($bdd, "locations");

    // fusion des 2 tableaux de logements ventes et locations
    $all_logements = array_merge($logements_ventes, $logements_locations);

    // filtre les logements par date de publication
    usort($all_logements, function($a, $b){
        return $b->getDate_publication() <=> $a->getDate_publication();
    });

    // suppression du logement dans la BD, affiche le message et rafraichis la page
    if (isset($_GET['delete_vente'])){
        $msg_sup = "Logement supprimé!";
        $id = $_GET['delete_vente'];
        $bdd->query("DELETE FROM ventes WHERE id=$id");
        $page = $_SERVER['PHP_SELF'];
        $sec = "2";
        header("Refresh: $sec; url=$page");
    }
    if (isset($_GET['delete_location'])){
        $msg_sup = "Logement supprimé!";
        $id = $_GET['delete_location'];
        $bdd->query("DELETE FROM locations WHERE id=$id");
        $page = $_SERVER['PHP_SELF'];
        $sec = "2";
        header("Refresh: $sec; url=$page");
    }
    ?>
    <div class="msg_container">
        <h3 class="text-danger text-center msg_sup"><?php echo $msg_sup; ?></h3>
    </div>
    
    <?php
    // boucle sur tout les logements pour l'affichage HTML, selon si logement ventes ou locations
    foreach ($all_logements as $logement) {
        //si la classe de logement = ventes, faire l'affichage
        if(get_class($logement) === "ventes"){
           ?> 
            <div class="card_container">
            <div class="card_img">
                <a href="<?= $logement->getUrl(); ?>"><img src="<?= $logement->getImage(); ?>" alt="image logement"></a>
            </div>
            <div class="card_info">
               <h3>À vendre</h3>
               <p class="card_date">Parution: <?= $logement->getDate_publication(); ?></p>
               <h3 class="card_address"><?= $logement->getAddress(); ?></h3>
               <p class="card_price"><?= $logement->getPrice(); ?> $</p>
               <p class="card_condo"><?php if ($logement->getCondo() === 1){
                           echo "Copropriété: oui";
                       } else{
                           echo "Copropriété: non";
                       }
                       ?>
               </p>
               <a href="<?= $logement->getUrl(); ?>" class="btn btn-info">Edit</a>
               <a href="index.php?delete_vente=<?= $logement->getId(); ?>" class="btn btn-danger">Supprimer</a>
            </div>
            </div>
        <?php
        } else {
            // sinon afficher logement location
            ?>
            <div class="card_container">
                <div class="card_img">
                    <a href="<?= $logement->getUrl(); ?>"><img src="<?= $logement->getImage(); ?>" alt="logement"></a>
                </div>
                <div class="card_info">
                    <h3>À louer</h3>
                    <p class="card_parution">Parution: <?= $logement->getDate_publication(); ?></p>
                    <p class="card_possession">Possession: <?= $logement->getDate_possession(); ?></p>
                    <h3 class="card_address"><?= $logement->getAddress(); ?></h3>
                    <p class="card_price"><?= $logement->getPrice(); ?> $</p>
                    <p class="card_loue"><?php if($logement->getProfessional() === 1){
                                echo "Loué par: Professionnel";
                            } else{
                                echo "Loué par: Particulier";
                            }
                            ?>
                    </p>
                    <a href="<?= $logement->getUrl(); ?>" class="btn btn-info">Edit</a>
                    <a href="index.php?delete_location=<?= $logement->getId(); ?>" class="btn btn-danger">Supprimer</a>
                </div>
            </div>
            <?php
        }
    }
}


// affichage des logements en ventes
function renderVentes($bdd){
    $msg_sup = "";
    
    // va chercher logements ventes
    $logements_ventes = getLogements($bdd, "ventes");
    
    // filtre par date de publication
    usort($logements_ventes, function($a, $b){
        return $b->getDate_publication() <=> $a->getDate_publication();
    });
    
    // suppression du logement dans la BD, affiche le message et rafraichis la page
    if (isset($_GET['delete_vente'])){
        $msg_sup = "Logement supprimé!";
        $id = $_GET['delete_vente'];
        $bdd->query("DELETE FROM ventes WHERE id=$id");
        $page = $_SERVER['PHP_SELF'];
        $sec = "2";
        header("Refresh: $sec; url=$page");
    }
    ?>
    <div class="msg_container">
        <h3 class="text-danger text-center msg_sup"><?php echo $msg_sup; ?></h3>
    </div>

    <?php

    // pour chaque logements ventes faire l'affichage HTML
     foreach ($logements_ventes as $logement_vente){
     ?>
     <div class="card_container">
         <div class="card_img">
             <a href="<?= $logement_vente->getUrl(); ?>"><img src="<?= $logement_vente->getImage(); ?>" alt="logement"></a>
         </div>
         <div class="card_info">
             <h3>À vendre</h3>
             <p class="card_date">Parution: <?= $logement_vente->getDate_publication(); ?></p>
             <h3 class="card_address"><?= $logement_vente->getAddress(); ?></h3>
             <p class="card_price"><?= $logement_vente->getPrice(); ?> $</p>
             <p class="card_condo"><?php if($logement_vente->getCondo() === 1){
                         echo "Copropriété: oui";
                     } else{
                         echo "Copropriété: non";
                     }
                     ?>
             </p>
             <a href="<?= $logement_vente->getUrl(); ?>" class="btn btn-info">Edit</a>
             <a href="ventes.php?delete_vente=<?= $logement_vente->getId(); ?>" class="btn btn-danger">Supprimer</a>
         </div>
     </div>
     <?php
    }
}


// affichage des logements en locations
function renderLocations($bdd){
    $msg_sup = "";

    //va chercher les logements locations
    $logements_locations = getLogements($bdd, "locations");

    // filtre par date de publication
    usort($logements_locations, function($a, $b){
        return $b->getDate_publication() <=> $a->getDate_publication();
    });

    // suppression du logement dans la BD, affiche le message et rafraichis la page
    if (isset($_GET['delete_location'])){
        $msg_sup = "Logement supprimé!";
        $id = $_GET['delete_location'];
        $bdd->query("DELETE FROM locations WHERE id=$id");
        $page = $_SERVER['PHP_SELF'];
        $sec = "2";
        header("Refresh: $sec; url=$page");
    }
    ?>

    <div class="msg_container">
        <h3 class="text-danger text-center msg_sup"><?php echo $msg_sup; ?></h3>
    </div>

    <?php

    // pour chaque logements ventes faire l'affichage HTML
    foreach ($logements_locations as $logement_location){
    ?>
        <div class="card_container">
            <div class="card_img">
                <a href="<?= $logement_location->getUrl(); ?>"><img src="<?= $logement_location->getImage(); ?>" alt="logement"></a>
            </div>
            <div class="card_info">
                <h3>À louer</h3>
                <p class="card_parution">Parution: <?= $logement_location->getDate_publication(); ?></p>
                <p class="card_possession">Possession: <?= $logement_location->getDate_possession(); ?></p>
                <h3 class="card_address"><?= $logement_location->getAddress(); ?></h3>
                <p class="card_price"><?= $logement_location->getPrice(); ?> $</p>
                <p class="card_loue"><?php if($logement_location->getProfessional() === 1){
                            echo "Loué par: Professionnel";
                        } else{
                            echo "Loué par: Particulier";
                        }
                        ?>
                </p>
                <a href="<?= $logement_location->getUrl(); ?>" class="btn btn-info">Edit</a>
                <a href="locations.php?delete_location=<?= $logement_location->getId(); ?>" class="btn btn-danger">Supprimer</a>
            </div>
        </div>
    <?php
    }
}
?>
