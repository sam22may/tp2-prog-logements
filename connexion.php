<?php
// informations pour la connexion a la BD
$dbHost = 'localhost';
$dbName = 'tp2-immologi';
$dbUser = 'root';
$dbPassword = '';
$dbCharset = 'utf8mb4';

$dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,    
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,     
    PDO::ATTR_EMULATE_PREPARES   => false
    ];
    
try {
    $bdd = new PDO($dsn, $dbUser, $dbPassword, $options);
} catch (PDOException $e) {
    die('Erreur : ' . $e->getMessage());
}