<?php
    include "./connexion.php";
    include "./functions.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./styles/css/style.css">
    <title>TP2 - ventes et locations Logements</title>
</head>

<body>
<header class="mb-5">
        <h1 class="text-center">Agence Immologi</h1>
        <h2 class="text-danger text-center">Ventes et locations de logements</h2>
        <nav class="d-flex justify-content-center">
            <div class="nav-item dropdown text-center navbtn">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">Voir</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/">Tous</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="ventes.php">À vendre</a>
                    <a class="dropdown-item" href="locations.php">À louer</a>
                </div>
            </div>
            <div class="nav-item dropdown text-center navbtn">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">Ajouter</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="ajouter_vente.php">Pour vendre</a>
                    <a class="dropdown-item" href="ajouter_location.php">Pour louer</a>
                </div>
            </div>
        </nav>
    </header>
    <main>
        <div class="all_card">
            <?php
                renderVentes($bdd);
            ?>
                
        </div>

    </main>

    <footer>

    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

</body>

</html>