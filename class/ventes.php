<?php

class ventes {

    private $id;
    private $address;
    private $image;
    private $price;
    private $date_publication;
    private $condo;

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     */ 
    public function setId($id)
    {
        return $this->id = $id;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of address
     */ 
    public function setAddress($address)
    {
       return $this->address = $address;
    }

    /**
     * Get the value of image
     */ 
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of image
     */ 
    public function setImage($image)
    {
        return $this->image = $image;
    }

    /**
     * Get the value of price
     */ 
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of price
     */ 
    public function setPrice($price)
    {
        return $this->price = $price;
    }

    /**
     * Get the value of date_publication
     */ 
    public function getDate_publication()
    {
        return $this->date_publication;
    }

    /**
     * Set the value of date_publication
     */ 
    public function setDate_publication($date_publication)
    {
        return $this->date_publication = $date_publication;
    }

    /**
     * Get the value of condo
     */ 
    public function getCondo()
    {
        return $this->condo;
    }

    /**
     * Set the value of condo
     */ 
    public function setCondo($condo)
    {
        return $this->condo = $condo;
    }

    /**
     * Get the url for edit
     */
    public function getUrl() 
    {
      return 'edit_vente.php?id=' . $this->id;
    }
}