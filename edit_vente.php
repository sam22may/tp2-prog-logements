<?php
    include "./connexion.php";
    include "./functions.php";

    $error_address = "";
    $error_price = "";
    $error_date_pub = "";
    $error_date_poss = "";
    $send_msg = "";

    if(isset($_POST['submit'])){
        
        // validation si les champs sont vides avec message d'erreur00000000
        if(empty($_POST['address'])){
            $error_address = "Entrer une adresse";
        } else {
            $error_address = "";
        }

        if(empty($_POST['price'])){
            $error_price = "Entrer un prix";
        } else {
            $error_price = "";
        }
        if(empty($_POST['date'])){
            $error_date_pub = "Entrer une date de publication";
        } else {
            $error_date_pub = "";
        }


        try {
            $requete = $bdd->prepare("UPDATE ventes SET address = :address, image = :image, price = :price, date_publication = :date_publication, condo = :condo WHERE id = :id");
            $requete->execute([
                ':address' => $_POST['address'],
                ':image' => $_POST['image'],
                ':price' => $_POST['price'],
                ':date_publication' => $_POST['date'],
                ':condo' => $_POST['condo'],
                ':id' => $_POST['id']
            ]);
            $send_msg = "Logement modifier avec succès !";
        } catch (PDOException $e) {
            // En cas d'erreur, on affiche un message
            // throw new PDOException($e->getMessage(), (int)$e->getCode());
            $send_msg = 'Erreur : '.$e->getMessage();
    
        }
        
    }

    // va chercher l'ID dans URL pour aller chercher le logement dans la BD
    $getId = $_GET['id'];
    $getId = $_GET['id'];
    $requete = $bdd->prepare('SELECT * FROM ventes WHERE id = :id');
    $requete->execute([
      'id' => $getId
    ]);
    $location = $requete->fetch();

    $bdd = null;
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./styles/css/style.css">
    <title>TP2 - ventes et locations Logements</title>
</head>

<body>
    <header class="mb-5">
        <h1 class="text-center">Agence Immologi</h1>
        <h2 class="text-danger text-center">Ventes et locations de logements</h2>
        <nav class="d-flex justify-content-center">
            <div class="nav-item dropdown text-center navbtn">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">Voir</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/">Tous</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="ventes.php">À vendre</a>
                    <a class="dropdown-item" href="locations.php">À louer</a>
                </div>
            </div>
            <div class="nav-item dropdown text-center navbtn">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">Ajouter</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="ajouter_vente.php">Pour vendre</a>
                    <a class="dropdown-item" href="ajouter_location.php">Pour louer</a>
                </div>
            </div>
        </nav>
    </header>
    <main>

        <h3 class="text-center h3">Modifier un logement pour la vente</h3>
        <h5 class="text-center text-success"><?php echo $send_msg; ?></h5>

        <div class="container conteneur-form">
            <form action="edit_vente.php?id=<?= $getId; ?>" method="POST">
            <input name="id" type="hidden" value="<?= (isset($location->id))? $location->id:''  ?>">
                <div class="form-group">
                    <label for="address">Adresse</label>
                    <input type="text" class="form-control" name="address" placeholder="Adresse" value="<?= (isset($location->address))? $location->address:''  ?>">
                    <span class="text-danger"><?php echo $error_address; ?></span>
                </div>
                <div class="form-group">
                    <label for="image">Choisir l'image</label>
                    <select name="image" id="image" class="form-control">
                        <option value="./images/vente/vente_condo_01.jpg">Condo 1</option>
                        <option value="./images/vente/vente_condo_02.jpg">Condo 2</option>
                        <option value="./images/vente/vente_maison_01.jpg">Maison 1</option>
                        <option value="./images/vente/vente_maison_02.jpg">Maison 2</option>
                        <option value="./images/vente/vente_maison_03.jpg">Maison 3</option>
                        <option value="./images/vente/vente_maison_04.jpg">Maison 4</option>
                        <option value="./images/vente/vente_maison_05.jpg">Maison 5</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Prix</label>
                    <input type="text" class="form-control" name="price" placeholder="Prix" value="<?= (isset($location->price))? $location->price:''  ?>">
                    <span class="text-danger"><?php echo $error_price; ?></span>
                </div>
                <div class="form-group">
                    <label for="date">Date de publication</label>
                    <input type="text" class="form-control" name="date" placeholder="ANNÉE-MOIS-JOUR" value="<?= (isset($location->date_publication))? $location->date_publication:''  ?>">
                    <span class="text-danger"><?php echo $error_date_pub; ?></span>
                </div>
                <div class="form-group">
                    <label for="condo">Copropriété ?</label>
                    <br>
                    <input type="radio" name="condo" id="oui" value="1">
                    <label for="oui">Oui</label>
                    <br>
                    <input type="radio" name="condo" id="non" value="0" checked>
                    <label for="non">Non</label>
                </div>
                

                <input type="submit" name="submit" class="btn btn-primary" value="Modifier">
            </form>
        </div>

    </main>

    <footer>

    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
</body>

</html>