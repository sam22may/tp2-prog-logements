-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 24 août 2021 à 17:02
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tp2-immologi`
--

-- --------------------------------------------------------

--
-- Structure de la table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `date_publication` date NOT NULL,
  `date_possession` date NOT NULL,
  `professional` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `locations`
--

INSERT INTO `locations` (`id`, `address`, `image`, `price`, `date_publication`, `date_possession`, `professional`) VALUES
(1, '12 rue du Paradis, Cancun', './images/location/location_07.jpg', '5000.00', '2020-08-05', '2021-08-02', 1),
(2, '66 avenue Moderne, Pékin', './images/location/location_05.jpg', '2100.00', '2019-12-26', '2021-05-12', 1),
(3, '1234 avenue Larrivière, Paris', './images/location/location_01.jpg', '1599.89', '2021-04-05', '2021-08-25', 0),
(4, '78 chemin du Lac, Shawinigan', './images/location/location_02.jpg', '789.00', '2021-06-01', '2021-07-01', 0),
(5, '986 avenue Provancher, Montréal', './images/location/location_03.jpg', '850.00', '2020-09-03', '2020-10-30', 0),
(6, '1222 de la Campagne, Québec', './images/location/location_01.jpg', '2331.00', '2021-07-22', '2021-08-01', 1),
(22, '78 de la prairie, Château-Richer', './images/location/location_01.jpg', '3231.00', '2021-08-22', '2021-12-01', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ventes`
--

DROP TABLE IF EXISTS `ventes`;
CREATE TABLE IF NOT EXISTS `ventes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `date_publication` date NOT NULL,
  `condo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ventes`
--

INSERT INTO `ventes` (`id`, `address`, `image`, `price`, `date_publication`, `condo`) VALUES
(1, '123 rue du logement, Québec', './images/vente/vente_maison_01.jpg', '250000.00', '2021-08-07', 0),
(2, '1020 rue du Capricorne, St-Constellation', './images/vente/vente_condo_02.jpg', '454123.92', '2021-08-03', 1),
(3, '9 rue de l\'aigle, St-Apollon', './images/vente/vente_condo_01.jpg', '653655.00', '2021-07-22', 1),
(4, '454 rue du Mais, St-Augustin', './images/vente/vente_maison_02', '678000.00', '2021-07-22', 0),
(5, '245 rue du Terrier, Bérouth', './images/vente/vente_maison_03.jpg', '455000.00', '2021-04-14', 0),
(8, '116 rue du chanvres', './images/vente/vente_maison_05.jpg', '3213.00', '1990-04-12', 1),
(35, '1234 alababa', './images/vente/vente_condo_01.jpg', '216000.00', '2021-11-22', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
